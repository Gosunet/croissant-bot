package main

import (
	"log"
	"net/http"

	p "github.com/gfeun/croissant-bot"
)

func main() {
	if err := http.ListenAndServe(":8080", http.HandlerFunc(p.Croissant)); err != nil {
		log.Fatal(err)
	}
}
