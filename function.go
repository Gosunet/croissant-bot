// Package p contains an HTTP Cloud Function.
package p

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/slack-go/slack"
)

// Croissant is the exported cloud function
func Croissant(w http.ResponseWriter, r *http.Request) {
	slackSigningSecret := os.Getenv("SLACK_SIGNING_SECRET")
	if slackSigningSecret == "" {
		log.Println("SLACK_VERIFICATION_TOKEN not set")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Oops, the bot is misconfigured ... Missing SLACK_SIGNING_SECRET env var"))
		return
	}

	verifier, err := slack.NewSecretsVerifier(r.Header, slackSigningSecret)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(constructBotResponse("Oops, the bot failed to create a signature verifier"))
		return
	}

	r.Body = ioutil.NopCloser(io.TeeReader(r.Body, &verifier))
	s, err := slack.SlashCommandParse(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(constructBotResponse("Oops, the bot failed to parse the incoming slash command"))
		return
	}

	if err = verifier.Ensure(); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write(constructBotResponse("Oops, the bot failed to verify incoming message signature"))
		return
	}

	switch s.Command {
	case "/croissant":
		// Get user name from API
		slackOAuthToken := os.Getenv("SLACK_OAUTH_TOKEN")
		if slackOAuthToken == "" {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write(constructBotResponse("Oops, the bot is misconfigured ... Missing SLACK_OAUTH_TOKEN env var"))
			return
		}

		api := slack.New(slackOAuthToken)
		userInfo, err := api.GetUserInfo(s.UserID)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write(constructBotResponse("Oh no, the bot failed to recover UserInfo from slack API..."))
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(constructBotResponse("Merci " + userInfo.RealName + ", tu régales !! #petit-dej #lock-ton-pc"))
	default:
		log.Println("Error: got wrong slack slash command:", s.Command)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(constructBotResponse("This bot is listening for /croissant not " + s.Command))
		return
	}
}

func constructBotResponse(msg string) []byte {
	b, err := json.Marshal(&slack.Msg{Text: msg, ResponseType: slack.ResponseTypeInChannel})
	if err != nil {
		log.Fatal("Error: json marshal failed:", err)
	}
	return b
}
